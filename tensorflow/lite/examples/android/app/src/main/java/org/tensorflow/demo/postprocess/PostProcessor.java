package org.tensorflow.demo.postprocess;

import android.os.SystemClock;
import android.util.Log;

import org.tensorflow.demo.Classifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static android.content.ContentValues.TAG;

public class PostProcessor extends DBSCAN{

    Batch batch;
    List<Batch> batches = new ArrayList<>(); // batches: store all latest batches in several frames.
    private long startTime;
    private static final long TIME_THRESHOLD = 650;//after optimization: 650 ms;
    private static final float OVERLAP_THRESHOLD = 0.25f;
    private static final int LIMIT = 100;


    /**
     * Batch: a class to store all Predictions of one frame, and its corresponding starting time
     */
    class Batch{
        List<Classifier.Recognition> predictions = new ArrayList<>();
        long startTime = 0;

        public Batch(List<Classifier.Recognition> predictions, long startTime) {
            this.predictions = predictions;
            this.startTime = startTime;
        }
    }

    public interface MyCallback {
        public void creditCardNumberDetected(String cardNumber);
    }


    /**
     * Go through the entire pipeline of Post-Process
     *
     * Step 1: Bathces frames up: addNewBatch;
     * Step 2: Filter batches into latest ones;
     * Step 3: Select predictions with Non Maximum Suppression: eliminate overlapped ones;
     * Step 4: Cluster detection boxes into several clusters with DBSCAN algorithm: penalize vertically
     * Step 5: Check Luhn algorithm on all clusters and return Card number that passes it;
     * Step 6: Return cc number via callback
     *
     * @param predictions A list of predictions to be added into batches
     * @param callback A callback function after the Post-process
     */
    public void process(List<Classifier.Recognition> predictions, MyCallback callback) {
        addNewBatch(predictions);
        batchFilter();
        getSelectedPredictions();
        String cardNumber = getCardNumber();
        callback.creditCardNumberDetected(cardNumber);
        Log.d(TAG, "Card Number is: " + cardNumber);
    }



    /**
     * Step 1: Bathces frames up, Add new batch to full batched list
     *
     * @param predictions Add a newly captured list of predictions on one frame into batches
     */
    private void addNewBatch(List<Classifier.Recognition> predictions) {
        Log.d(TAG, "Entering addNewBatch!");
        startTime = SystemClock.uptimeMillis();
        batch = new Batch(predictions, startTime);
        batches.add(batch);
    }



    /**
     * Step 2: Filter batches into latest ones: within TIME_THRESHOLD
     *
     * @return A list of filtered batches gathered from frames captured within TIME_THRESHOLD
     */
    private List<Batch> batchFilter() {
        Log.d(TAG, "Entering batchFilter!");
        long currentTime = SystemClock.uptimeMillis();
        long lowTime = currentTime - TIME_THRESHOLD;
        Log.d(TAG, "batch size before filtering " + batches.size());

        batches = batches.stream().filter(p -> p.startTime > lowTime).collect(Collectors.toList());

        // ************* for lower version of SDK ***************
        //        List<Batch> newBatches = new ArrayList<>();
        //        for(Batch batch: batches) {
        //            if(batch.startTime > lowTime) newBatches.add(batch);
        //        }
        //        batches = newBatches;
        // ************* for lower version of SDK ***************

        Log.d(TAG, "low time is " + lowTime);
        Log.d(TAG, "startTime is " + batches.get(0).startTime);
        Log.d(TAG, "time gap is " + (batches.get(0).startTime - lowTime));
        Log.d(TAG, "batch size after filtering " + batches.size()
                + " " + getNumbers(batches.get(0).predictions));
        return batches;
    }



    /**
     * Step 3: Select predictions with Non Maximum Suppression: eliminate overlapped ones;
     *
     * @return A list of prediction without overlapped ones.
     */
    private List<Classifier.Recognition> getSelectedPredictions() {

        Log.d(TAG, "Entering getSelectedPredictions!");

        // Combine all Detections together in latest batches(frames)
        List<Classifier.Recognition> combinedPredictions = new ArrayList<>();
        for(Batch each : batches) {
            combinedPredictions.addAll(each.predictions);
        }

        // Non Maximum Suppression: eliminate overlapped Detection boxes
        List<Classifier.Recognition> selectedPredictions = nonMaxSuppression(combinedPredictions, LIMIT, OVERLAP_THRESHOLD);
        Log.d(TAG, "selectedPredictions" + selectedPredictions.size());
        Log.d(TAG, "combinedPredictions" + combinedPredictions.size());
        return selectedPredictions;
    }




    /**
     * Step 4: Cluster detection boxes with DBSCAN algorithm
     *
     * @return A Map mapping from cluster labels into the list of predictions considered as within given cluster
     */
    private Map<Integer, List<Classifier.Recognition>> getClusters() {
        Log.d(TAG, "Entering getClusters!");

        List<Classifier.Recognition> selectedPredictions = getSelectedPredictions();
        float maxDistance = getMaxDistance(selectedPredictions);

        Log.d(TAG, "selectedPredictions all numbers: " + getNumbers(selectedPredictions));

        // Use DBScan algorithm to get cluster labels
        int[] clusterLabels = DBScan(selectedPredictions, maxDistance, 1);

        // Map from cluster label into all points in the cluster
        Map<Integer, List<Classifier.Recognition>> clustersMap = new HashMap<>();
        for(int i=0; i<clusterLabels.length; i++) {
            int curLabel = clusterLabels[i];
            if(!clustersMap.containsKey(curLabel)) {
                clustersMap.put(curLabel, new ArrayList<>());
            }
            clustersMap.get(curLabel).add(selectedPredictions.get(i));
            //clustersMap.getOrDefault(curLabel, new ArrayList<>()).add(selectedPredictions.get(i));
        }
        return clustersMap;
    }


    /**
     * Define a maximum distance to be the Threshold for predictions to be considered as within the same cluster
     *
     * @param selectedPredictions A list of predictions for calculating maximum distance
     * @return Calculated maximum distance
     */
    private float getMaxDistance(List<Classifier.Recognition> selectedPredictions) {
        Log.d(TAG, "Entering getMaxDistance!");
        float maxDistance = 0f;
        for(Classifier.Recognition selectedPrediction : selectedPredictions) {
            maxDistance += selectedPrediction.getLocation().height() * 3;
            Log.d(TAG, "width: " + selectedPrediction.getLocation().width() + " Height: " + selectedPrediction.getLocation().height());
        }
        maxDistance = maxDistance / selectedPredictions.size();
        Log.d(TAG, "Max distance! " + maxDistance + " width ");
        return maxDistance;
    }




    /**
     * Step 5: Check Luhn algorithm on all clusters and return Card number that passes it;
     *
     * @return CardNumber passed the Luhn algorithm
     */
    private String getCardNumber() {
        Log.d(TAG, "Entering getCardNumber");
        Map<Integer, List<Classifier.Recognition>> clustersMap = getClusters();
        String cardNumber = "";
        int count = 0;
        Log.d(TAG, "There are # of clusters on the card ! " + clustersMap.size());

        for(int label : clustersMap.keySet()) {
            List<Classifier.Recognition> curCluster = clustersMap.get(label);
            // Sort the cluster in order of location
            Collections.sort(curCluster, new Comparator<Classifier.Recognition>() {
                @Override
                public int compare(Classifier.Recognition r1, Classifier.Recognition r2) {
                    return (int) (r2.getLocation().bottom - r1.getLocation().bottom);
                }
            });

            Log.d(TAG, "current cluster numbers string: " + getNumbers(curCluster));
            if(checkLuhn(curCluster)) {
                cardNumber = getNumbers(curCluster);  // extract card number
                count ++;
            }
        }
        if(count > 1) return ""; // if there are more than 1 cluster pass the CheckSum on the card, then it's an invalid card
        return cardNumber;
    }




    /**
     * Extract numbers string from a list of recognitions
     *
     * @param recognitions a list of sorted detections to be parsed for number
     * @return Number parsed from the cluster
     */
    private String getNumbers(List<Classifier.Recognition> recognitions) {
        String numbers = "";
        for(Classifier.Recognition recognition : recognitions) {
            Log.d(TAG, "recognition.getLocation().bottom " + recognition.getLocation().bottom);
            numbers += recognition.getTitle();
        }
        return numbers;
    }

}
