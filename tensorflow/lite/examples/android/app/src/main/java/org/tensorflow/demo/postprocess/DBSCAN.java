package org.tensorflow.demo.postprocess;

import android.graphics.RectF;
import android.util.Log;

import org.tensorflow.demo.Classifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static android.content.ContentValues.TAG;

public class DBSCAN extends Helpers{

    /**
     * DBSCAN algorithm: Clustering detection boxes(nearest neighbors) into several clusters, penalize vertically
     *
     * Based on code from: https://en.wikipedia.org/wiki/DBSCAN
     *
     * @param points A list of detections detected from the card
     * @param maxDistance The distance threshold to be considered as nearest neighbors
     * @param minPoints Minimum number of points to be considered as a cluster
     * @return An array of cluster labels for each point
     */
    public int[] DBScan(List<Classifier.Recognition> points, float maxDistance, int minPoints) {
        Log.d(TAG, "Entering DBSCAN!");
        int curCluster = 0;
        int[] labels = new int[points.size()];
        Arrays.fill(labels, -2);    // -2 = Undefined , -1 = Noise; initialized all detections as undefined

        for (int i=0; i<points.size(); i++) {
            if(labels[i] == -2) {  // just go through 'undefined' (not gone through yet) points
                List<Integer> nearestNeighbors = findNearestNeighbors(i, points, labels, maxDistance);

                if(nearestNeighbors.size() >= minPoints) {  // points[i] has at least 'minPoints = 1' neighbors
                    curCluster ++;  // cluster id
                    labels[i] = curCluster;  // set cluster label for current point

                    // Consider all points (id) in current cluster as working points and continue find their
                    // neighbors and add into current cluster
                    Queue<Integer> workingPoints = new LinkedList<>();
                    workingPoints.addAll(nearestNeighbors);

                    // go through all neighbors and find their neighbors, and consider them as workingPoints
                    while(!workingPoints.isEmpty()) {
                        int curPointIdx = workingPoints.poll();
                        if(labels[curPointIdx] > 0) {
                            continue;
                        }
                        labels[curPointIdx] = curCluster;  // label this working point as current cluster

                        // look for nearest neighbors for 'curPointIdx', and add them as working points
                        List<Integer> newNeighbors = findNearestNeighbors(curPointIdx, points, labels, maxDistance);
                        if(newNeighbors.size() >= minPoints) {
                            workingPoints.addAll(newNeighbors);
                        }

                    }
                } else {
                    labels[i] = -1;  // if no neighbors, then considered as noise
                }
            }
        }
        return labels;   // labels
    }



    /**
     * Find the nearest neighbors for given Point[pointIndex]
     *
     * @param pointIndex Index of given detection box
     * @param points A list of detections detected from the card
     * @param labels An array of cluster labels for each point
     * @param maxDistance The distance threshold to be considered as nearest neighbors
     * @return A list of point indices marked as nearest neighbors for given point.
     */
    private List<Integer> findNearestNeighbors(int pointIndex, List<Classifier.Recognition> points, int[] labels,
                                               float maxDistance) {
        Log.d(TAG, "Entering findNearestNeighbors!");
        Classifier.Recognition point = points.get(pointIndex);
        RectF pointRect = point.getLocation();
        List<Integer> nearestNeighbors = new ArrayList<>();

        for(int i=0; i<points.size(); i++) {
            if(labels[i] < 0 && i != pointIndex) {

                // call distanceBtRects from Helpers and get distance between two points
                float dist = distanceBtRects(pointRect, points.get(i).getLocation());
                if(dist <= maxDistance) {
                    nearestNeighbors.add(i);
                }
            }
        }
        return nearestNeighbors;
    }


}
