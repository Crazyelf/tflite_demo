package org.tensorflow.demo.postprocess;

import android.graphics.RectF;
import android.util.Log;

import org.tensorflow.demo.Classifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.content.ContentValues.TAG;

public class Helpers {

    /**
     * Non Maximum Suppression algorithm: eliminate overlapped detection boxes
     * from Predictions captured from several frames
     *
     * Based on code from
     * https://github.com/tensorflow/tensorflow/blob/master/tensorflow/core/kernels/non_max_suppression_op.cc
     * https://github.com/hollance/YOLO-CoreML-MPSNNGraph
     *
     * @param fullPredictions A list of detections captured from several frames within eg. 0.65s.
     * @param limit Upper limit number of detections to be selected
     * @param threshold Threshold for Intersection Over Union for detections to be selected
     * @return A list of detections selected after eliminating overlapped ones
     */
    public List<Classifier.Recognition> nonMaxSuppression(
            List<Classifier.Recognition> fullPredictions,
            int limit,
            float threshold) {

        Log.d(TAG, "Non Maximum Suppression!!!");

        Collections.sort(fullPredictions, new Comparator<Classifier.Recognition>() {
            @Override
            public int compare(Classifier.Recognition r1, Classifier.Recognition r2) {
                return (int) (r1.getConfidence() - r2.getConfidence());
            }
        });

        List<Classifier.Recognition> selectedPredictions = new ArrayList<Classifier.Recognition>();

        int fullSize = fullPredictions.size();
        int numActive = fullSize;
        boolean[] active = new boolean[fullSize];
        Arrays.fill(active, true);

        outerloop:
        for(int i=0; i<fullSize; i++) {
            if(active[i]) {
                selectedPredictions.add(fullPredictions.get(i));
                if(selectedPredictions.size() >= limit) break;

                for(int j=i+1; j<fullSize; j++) {
                    if(active[j]) {
                        RectF rect1 = fullPredictions.get(i).getLocation();
                        RectF rect2 = fullPredictions.get(j).getLocation();

                        // Check Intersection / Union against Threshold
                        if(IOU(rect1, rect2) > threshold) {
                            active[j] = false;  // when set to false, will not be selected.
                            numActive -= 1;
                            if(numActive <= 0) {
                                break outerloop;
                            }
                        }
                    }
                }
            }
        }
        return selectedPredictions;
    }




    /**
     * Calculate Intersection over Union ratio for two detection boxes: To be used in Non Maximum Suppression algorithm
     *
     * @param r1 Detection box 1
     * @param r2 Detection box 2
     * @return Intersection over Union ratio for these two detection boxes
     */
    private float IOU(RectF r1, RectF r2) {
        float area1 = r1.width() * r1.height();
        float area2 = r2.width() * r2.height();
        if(area1 <=0 || area2 <= 0) return 0;

        float intersectionLeft = Math.max(r1.left, r2.left);
        float intersectionRight = Math.min(r1.right, r2.right);
        float intersectionTop = Math.max(r1.top, r2.top);
        float intersectionBottom = Math.min(r1.bottom, r2.bottom);

        float intersectionWidth = Math.max(intersectionRight - intersectionLeft, 0);
        float intersectionHeight = Math.max(intersectionBottom - intersectionTop, 0);

        float intersectionArea = intersectionWidth * intersectionHeight;
        float unionArea = area1 + area2 - intersectionArea;

        return intersectionArea / unionArea;
    }



    /**
     * Compute distances between two detection boxes: to be used in selecting nearest neighbor
     * @param rect1 Detection box 1
     * @param rect2 Detection box 2
     * @return Distance between two detection boxes, distance will be penalized vertically!
     */
    public float distanceBtRects(RectF rect1, RectF rect2) {

        double dist = Math.pow(Math.pow(rect1.left-rect2.left, 2)
                + Math.pow(rect1.top-rect2.top, 2), 0.5);

        // Penalize for Y axis: here left corresponds to top of the number!
        if(Math.abs(rect1.left-rect2.left) > 0.5* Math.abs(rect1.top-rect2.top)) {
            return (float) 1000;  // penalize for Y axis.
        }

        return (float) dist;
    }




    /**
     * Check whether the card number is valid or not with Luhn algorithm(check sum)
     *
     * @param cluster A list of detections to be considered as in the same cluster
     * @return A boolean: whether the cluster of numbers pass the Luhn algorithm or not
     */
    public boolean checkLuhn(List<Classifier.Recognition> cluster) {
        Log.d(TAG, "Luhn checkSum algorithm !!!");

        if(cluster.size() < 15 || cluster.size() > 16) {
            return false;
        }
        // get predicted number for first digit, check validity for first digit;
        int fisrtDigit = Integer.parseInt(cluster.get(0).getTitle());
        if(fisrtDigit == 0 || fisrtDigit == 1 || fisrtDigit == 2 ||
                fisrtDigit == 7 || fisrtDigit == 8 || fisrtDigit == 9) {
            return false;
        }
        int sum = 0;
        int parity = cluster.size() % 2;
        for(int i=0; i<cluster.size(); i++) {
            int digit = Integer.parseInt(cluster.get(i).getTitle());

            if(i % 2 == parity) {
                digit *= 2;
                if(digit > 9) {
                    digit -= 9;
                }
            }
            sum += digit;
        }
        return sum % 10 == 0;
    }
}
